<?php

namespace App\Helper;

class TextHelper
{
    public function firstUpper($text): string
    {
        $first = mb_substr($text,0,1, 'UTF-8');
        $last = mb_substr($text,1);
        $first = mb_strtoupper($first, 'UTF-8');
        $last = mb_strtolower($last, 'UTF-8');

        return $first.$last;
    }

}