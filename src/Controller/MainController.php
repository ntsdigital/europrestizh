<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_index")
     */
    public function index()
    {
        return $this->json('Welcome Europrestizh test', 200, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/api", name="api_index")
     */
    public function api()
    {
        return $this->json('Welcome Europrestizh API interface test', 200, ["Content-Type" => "application/json"]);
    }
}