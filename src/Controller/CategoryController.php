<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Characteristic;
use App\Helper\TextHelper;
use App\Repository\CategoryRepository;
use App\Repository\CharacteristicRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/api", name="api_")
 */
class CategoryController extends AbstractController
{
    /**
     * @var TextHelper
     */
    private $textHelper;

    public function __construct(TextHelper $textHelper)
    {
        $this->textHelper = $textHelper;
    }

    /**
     * @Route("/category", name="category_all", methods={"GET"})
     */
    public function getAll(CategoryRepository $categoryRepository): JsonResponse
    {
        $categories = $categoryRepository->findAll();

        return $this->json(['categories' => $categories], 200, ["Content-Type" => "application/json"], [
            'groups' => ['category']
        ]);
    }

    /**
     * @Route("/category", name="category_create", methods={"POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function create(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $body = json_decode($request->getContent());
        $category = new Category();

        $category
            ->setName($body->name)
            ;

        $em->persist($category);
        $em->flush();

        return $this->json('Category successful create', 200, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/category/{id}", name="category_edit", methods={"PATCH"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function edit($id, Request $request, CategoryRepository $categoryRepository, EntityManagerInterface $em): JsonResponse
    {
        $body = json_decode($request->getContent());
        $category = $categoryRepository->findOneBy(['id' => $id]);

        if ($category)
        {
            $category
                ->setName($body->name)
            ;

            $em->persist($category);
            $em->flush();
            return $this->json('Category successful edit', 200, ["Content-Type" => "application/json"]);
        }
        return $this->json('Category not found', 404, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/category/{id}", name="category_delete", methods={"DELETE"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function delete($id, CategoryRepository $categoryRepository, EntityManagerInterface $em): JsonResponse
    {
        $category = $categoryRepository->findOneBy(['id' => $id]);

        if ($category)
        {
            $em->remove($category);
            $em->flush();
            return $this->json('Category successful delete', 200, ["Content-Type" => "application/json"]);
        }

        return $this->json('Category not found', 404, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/category/{id}/add_characteristic", name="category_add_characteristic", methods={"POST"})
     */
    public function addCharacteristic($id, Request $request, CategoryRepository $categoryRepository, EntityManagerInterface $em): JsonResponse
    {
        $body = json_decode($request->getContent());
        $category = $categoryRepository->findOneBy(['id' => $id]);

        if ($category)
        {
            foreach ($category->getCharacteristics() as $characteristic)
            {
                if (strcasecmp($characteristic->getName(), $this->textHelper->firstUpper($body->name)) === 0)
                {
                    return $this->json('This characteristic already exist in the category', 409, ["Content-Type" => "application/json"]);
                }
            }

            $characteristic = new Characteristic();
            $characteristic
                ->setName($this->textHelper->firstUpper($body->name))
                ->addCategory($category)
                ;
            $em->persist($characteristic);
            $em->flush();

            return $this->json('Characteristic category successful add', 200, ["Content-Type" => "application/json"]);
        }

        return $this->json('Category not found', 404, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/category/{id}/characteristics", name="category_get_characteristics", methods={"GET"})
     */
    public function getCharacteristic($id, CategoryRepository $categoryRepository): JsonResponse
    {
        $category = $categoryRepository->findOneBy(['id' => $id]);

        if ($category)
        {

            if ($category->getCharacteristics())
            {
                return $this->json(['characteristics' => $category->getCharacteristics()], 200, ["Content-Type" => "application/json"], [
                    'groups' => ['category']
                ]);
            } else {
                return $this->json('Characteristic by category not found', 404, ["Content-Type" => "application/json"]);
            }
        }

        return $this->json('Category not found', 404, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/category/{id}/products", name="category_get_products", methods={"GET"})
     */
    public function getProducts($id, CategoryRepository $categoryRepository): JsonResponse
    {
        $category = $categoryRepository->findOneBy(['id' => $id]);

        if ($category)
        {
            if (count($category->getProducts()) > 0)
            {
                return $this->json(['products' => $category->getProducts()], 200, ["Content-Type" => "application/json"], [
                    'groups' => ['category']
                ]);
            } else {
                return $this->json('Category not implement products', 404, ["Content-Type" => "application/json"]);
            }
        }

        return $this->json('Category not found', 404, ["Content-Type" => "application/json"]);
    }
}