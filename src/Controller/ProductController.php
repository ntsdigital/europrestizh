<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\CharacteristicRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product_create", methods={"POST"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function create(Request $request, CategoryRepository $categoryRepository, CharacteristicRepository $characteristicRepository, EntityManagerInterface $em): JsonResponse
    {
        $body = json_decode($request->getContent());

        if ($body->category)
        {
            $category = $categoryRepository->findOneBy(['id' => $body->category]);

            if ($category)
            {
                $characteristics = [];

                foreach ($body->characteristics as $key => $value)
                {
                    $characteristic = $characteristicRepository->findOneBy(['id' => intval($key)]);

                    if ($characteristic)
                    {
                        $characteristics[] = [$characteristic->getName() => $value];
                    } else {
                        return $this->json('Category not implement characteristics', 424, ["Content-Type" => "application/json"]);
                    }
                }

                $product = new Product();

                $product
                    ->setName($body->name)
                    ->setPrice($body->price)
                    ->setCategory($category)
                    ->setCharacteristics($characteristics)
                    ;

                $em->persist($product);
                $em->flush();

                return $this->json('Product success create', 200, ["Content-Type" => "application/json"]);

            } else {
                return $this->json('Category not found', 404, ["Content-Type" => "application/json"]);
            }
        }

        return $this->json('Category not found', 404, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/product/{id}", name="product_edit", methods={"PATCH"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function edit($id, Request $request, ProductRepository $productRepository, CategoryRepository $categoryRepository, CharacteristicRepository $characteristicRepository, EntityManagerInterface $em)
    {
        $body = json_decode($request->getContent());
        $product = $productRepository->findOneBy(['id' => $id]);

        if ($product)
        {
            if ($body->category)
            {
                $category = $categoryRepository->findOneBy(['id' => $body->category]);

                if ($category)
                {
                    $characteristics = [];

                    foreach ($body->characteristics as $key => $value)
                    {
                        $characteristic = $characteristicRepository->findOneBy(['id' => intval($key)]);

                        if ($characteristic)
                        {
                            $characteristics[] = [$characteristic->getName() => $value];
                        } else {
                            return $this->json('Category not implement characteristics', 424, ["Content-Type" => "application/json"]);
                        }
                    }

                    $product
                        ->setName($body->name)
                        ->setPrice($body->price)
                        ->setCategory($category)
                        ->setCharacteristics($characteristics)
                    ;

                    $em->persist($product);
                    $em->flush();

                    return $this->json('Product success update', 200, ["Content-Type" => "application/json"]);

                } else {
                    return $this->json('Category not found', 404, ["Content-Type" => "application/json"]);
                }
            }
        }

        return $this->json('Product not found', 404, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/product/{id}", name="product_delete", methods={"DELETE"})
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function delete($id, ProductRepository $productRepository, EntityManagerInterface $em): JsonResponse
    {
        $product = $productRepository->findOneBy(['id' => $id]);

        if ($product)
        {
            $em->remove($product);
            $em->flush();

            return $this->json('Product success delete', 200, ["Content-Type" => "application/json"]);
        }

        return $this->json('Product not found', 404, ["Content-Type" => "application/json"]);
    }
}