<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

/**
 * @Route("/api/auth", name="api_auth_")
 */
class AuthController extends AbstractController
{
    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    public function __construct(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }
    
    /**
     * @Route("/login", name="login", methods={"POST"})
     */
    public function login(Request $request, UserRepository $userRepository, UserPasswordHasherInterface $password_hasher): JsonResponse
    {
        $body = json_decode($request->getContent());
        $user = $userRepository->findOneBy(['email' => $body->username]);

        if ($user || $password_hasher->isPasswordValid($user, $body->password))
        {
            $token = $this->jwtManager->create($user);

            return new JsonResponse(['token' => $token]);
        }

        throw new BadCredentialsException();
    }

    /**
     * @Route("/register", name="register", methods={"POST"})
     */
    public function register(Request $request, UserRepository $userRepository, UserPasswordHasherInterface $password_hasher, EntityManagerInterface $em): JsonResponse
    {
        $body = json_decode($request->getContent());
        $candidate = $userRepository->findOneBy(['email' => $body->email]);

        if (!$candidate)
        {
            $user = new User();
            $plain_password = $body->password;
            $hashed_password = $password_hasher->hashPassword($user, $plain_password);
            $user
                ->setEmail($body->email)
                ->setUsername($body->email)
                ->setPassword($hashed_password)
                ->setIsActive(true)
                ;

            $em->persist($user);
            $em->flush();
        }

        return $this->json('Такой пользователь уже зарегистрирован', 409, ["Content-Type" => "application/json"]);
    }

}