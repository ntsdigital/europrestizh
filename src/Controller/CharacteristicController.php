<?php

namespace App\Controller;

use App\Helper\TextHelper;
use App\Repository\CharacteristicRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class CharacteristicController extends AbstractController
{
    /**
     * @var TextHelper
     */
    private $textHelper;

    public function __construct(TextHelper $textHelper)
    {
        $this->textHelper = $textHelper;
    }

    /**
     * @Route("/characteristic/{id}", name="characteristic_edit", methods={"PATCH"})
     */
    public function edit($id, Request $request, CharacteristicRepository $characteristicRepository, EntityManagerInterface $em): JsonResponse
    {
        $body = json_decode($request->getContent());
        $characteristic = $characteristicRepository->findOneBy(['id' => $id]);

        if ($characteristic)
        {
            $characteristic
                ->setName($this->textHelper->firstUpper($body->name))
                ;

            $em->persist($characteristic);
            $em->flush();

            return $this->json('Characteristic category successful edit', 200, ["Content-Type" => "application/json"]);
        }

        return $this->json('Characteristic not found', 404, ["Content-Type" => "application/json"]);
    }

    /**
     * @Route("/characteristic/{id}", name="characteristic_delete", methods={"DELETE"})
     */
    public function delete($id, CharacteristicRepository $characteristicRepository, EntityManagerInterface $em): JsonResponse
    {
        $characteristic = $characteristicRepository->findOneBy(['id' => $id]);

        if ($characteristic)
        {
            $em->remove($characteristic);
            $em->flush();

            return $this->json('Characteristic category successful delete', 200, ["Content-Type" => "application/json"]);
        }

        return $this->json('Characteristic not found', 404, ["Content-Type" => "application/json"]);
    }
}