<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Repository\UserRepository;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CategoryTest extends ApiTestCase
{
    public string $email= 'test@test.ru';

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testAddCategory()
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail($this->email);
        $client->loginUser($testUser);

        $client->request('POST', '/api/category', ['body' => json_encode(['name' => 'Category 1'])]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testGetCategories()
    {
        $client = static::createClient();
        $client->request('GET', '/api/category');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent(), 'categories');
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function editCategory()
    {
        $client = static::createClient();
        $client->request('PATCH', '/api/category/1', ['body' => json_encode(['name' => 'Category with new name'])]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function deleteCategory()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/category/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
