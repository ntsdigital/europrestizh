<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Repository\UserRepository;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ProductTest extends ApiTestCase
{
    public string $email= 'test@test.ru';

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testAddProduct(): void
    {
        $product = [
            'name' => 'Product 1',
            'category' => 1,
            'price' => 100,
            'characteristics' => [
                1 => 'Красный',
                2 => 32
            ]
        ];

        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail($this->email);
        $client->loginUser($testUser);

        $client->request('POST', '/api/product', ['body' => json_encode($product)]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testEditProduct()
    {
        $product = [
            'name' => 'Product 1',
            'category' => 1,
            'price' => 100,
            'characteristics' => [
                1 => 'Красный',
                2 => 32
            ]
        ];

        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail($this->email);
        $client->loginUser($testUser);

        $client->request('POST', '/api/product', ['body' => json_encode($product)]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }



    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testGetProducts(): void
    {
        $client = static::createClient();
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail($this->email);
        $client->loginUser($testUser);
        $client->request('GET', '/api/category/1/products');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}