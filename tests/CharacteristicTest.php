<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CharacteristicTest extends ApiTestCase
{
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testAddCharacteristic()
    {
        $client = static::createClient();
        $client->request('POST', '/api/category/1/add_characteristic', ['body' => json_encode(['name' => 'Цвет'])]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testEditCharacteristic()
    {
        $client = static::createClient();
        $client->request('PATCH', '/api/characteristic/1', ['body' => json_encode(['name' => 'Производитель'])]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testGetCharacteristicByCategory()
    {
        $client = static::createClient();
        $client->request('GET', '/api/category/1/characteristics');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertJson($client->getResponse()->getContent(), 'characteristics');
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testDeleteCharacteristic()
    {
        $client = static::createClient();
        $client->request('DELETE', '/api/characteristic/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}