<?php

namespace App\Tests;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class AuthTest extends ApiTestCase
{
    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testRegisterUser()
    {
        $user = [
          'email' => 'test@test.ru',
          'password' => 'test123'
        ];

        $client = static::createClient();
        $client->request('POST', '/api/auth/register', ['body' => json_encode($user)]);

        $this->assertEquals(200, $client->getResponse()->getContent());
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function testAuthUser()
    {
        $user = [
            'username' => 'test@test.ru',
            'password' => 'test123'
        ];

        $client = static::createClient();
        $client->request('POST', '/api/auth/login', ['body' => json_encode($user)]);

        $this->assertJson($client->getResponse()->getContent(), 'token');
    }

}